# -*- coding: utf-8 -*-

from openerp import models, fields, api

class City(models.Model):
    _name = 'res.city'

    name = fields.Char(string='Nombre')
    departamento = fields.Many2one("res.country.state", string="Departamento")
    codigo = fields.Integer(string="Codigo")
    zona=fields.Char(string='Zona')