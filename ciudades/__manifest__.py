# -*- coding: utf-8 -*-
{
    'name': "Ciudades",

    'summary': """
       Crear Modelo de ciudades relacionadas a los departamentos
       """,

    'description': """
      Crear Modelo de ciudades relacionadas a los departamentos
        
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    'category': 'Sale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        "views/res_city_view.xml",
    ],
}
