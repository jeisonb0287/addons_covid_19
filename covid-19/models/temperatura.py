# -*- coding: utf-8 -*-

from odoo import models, fields, api

class temperatura_covid(models.Model):
    _name = 'temperatura'

    date = fields.Datetime(string='Fecha',required=True,default=fields.Datetime.now())
    empleado_id = fields.Many2one('hr.employee',string="Empleado",required=True)
    temperatura = fields.Float(string='Temperatura',digits=(16, 2))
    type = fields.Selection(string='Tipo', selection=[('ing', 'Ingreso'), ('sal', 'Salida'), ],
                                     required=True, track_visibility='onchange')
